package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.EstadoCivilEnum;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Inheritance( strategy = InheritanceType.JOINED )
public class UsuarioEntity {

    @Id
    @SequenceGenerator( name = "SEQ_USUARIO", sequenceName = "SEQ_USUARIO" )
    @GeneratedValue( generator = "SEQ_USUARIO", strategy = GenerationType.SEQUENCE )
    private int id;
    private String nome;
    @Column( length = 11)
    private String cpf;
    @Column ( length = 8)
    private char dataNascimento;

    @OneToMany( mappedBy = "usuario")
    private List<ContaClienteEntity> contas;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_ENDERECO" )
    private EnderecoEntity endereco;

    @Enumerated( EnumType.STRING )
    private EstadoCivilEnum estadoCivil;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public char getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(char dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public EnderecoEntity getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
        this.endereco = endereco;
    }

    public EstadoCivilEnum getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
}
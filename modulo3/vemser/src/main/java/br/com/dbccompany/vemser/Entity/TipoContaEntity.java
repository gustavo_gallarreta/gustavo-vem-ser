package br.com.dbccompany.vemser.Entity;

import org.apache.catalina.LifecycleState;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.util.List;

@Entity
public class TipoContaEntity {

    @Id
    @SequenceGenerator( name = "SEQ_TIPO_CONTA", sequenceName = "SEQ_TIPO_CONTA" )
    private int id;
    private String nome;

    @OneToMany( mappedBy = "tipoConta")
    private List<ContaEntity> contas;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

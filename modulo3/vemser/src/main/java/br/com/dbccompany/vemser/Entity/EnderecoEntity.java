package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;

@Entity
public class EnderecoEntity {

    @Id
    @SequenceGenerator( name = "SEQ_ENDERECO", sequenceName = "SEQ_ENDERECO" )
    @GeneratedValue( generator = "SEQ_ENDERECO", strategy = GenerationType.SEQUENCE )
    private int id;
    private String logradouro;
    private int numero;
    private String complemento;
    private String cep;
    private String bairro;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_CIDADE" )
    private CidadeEntity cidade;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_AGENCIA" )
    private AgenciaEntity agencia;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_USUARIO" )
    private UsuarioEntity usuario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public CidadeEntity getCidade() {
        return cidade;
    }

    public void setCidade(CidadeEntity cidadeEntity) {
        this.cidade = cidadeEntity;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public UsuarioEntity getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioEntity usuario) {
        this.usuario = usuario;
    }
}

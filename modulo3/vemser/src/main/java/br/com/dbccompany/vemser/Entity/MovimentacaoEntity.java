package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.TipoMovimentacaoEnum;

import javax.persistence.*;

@Entity
public class MovimentacaoEntity {

    @Id
    @SequenceGenerator(name = "SEQ_MOVIMENTO", sequenceName = "SEQ_MOVIMENTO")
    @GeneratedValue(generator = "SEQ_MOVIMENTO", strategy = GenerationType.SEQUENCE)
    private int id;
    private double valor;

    @Enumerated(EnumType.STRING)
    private TipoMovimentacaoEnum tipoMovimentacao;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumns({
            @JoinColumn( name = "id_conta" ),
            @JoinColumn( name = "id_tipo_conta" )
    })
    private ContaEntity conta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public TipoMovimentacaoEnum getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacaoEnum tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public ContaEntity getConta() {
        return conta;
    }

    public void setConta(ContaEntity conta) {
        this.conta = conta;
    }
}
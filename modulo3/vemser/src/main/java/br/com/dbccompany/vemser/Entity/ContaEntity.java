package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContaEntity {

    @EmbeddedId
    private ContaEntityId id;
    private int codigo;
    private double saldo;

    @ManyToOne( cascade = CascadeType.ALL )
    @MapsId( "ID_TIPO_CONTA" )
    @JoinColumn( name = "ID_TIPO_CONTA", nullable = false )
    private TipoContaEntity tipoConta;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_AGENCIA" )
    private AgenciaEntity agencia;

    @OneToMany( mappedBy = "conta")
    private List<MovimentacaoEntity> movimentacoes;

    @OneToMany( mappedBy = "conta")
    private List<ContaClienteEntity> contas;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable(name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn( name = "id_conta" ), @JoinColumn( name = "id_tipo_conta" ) },
            inverseJoinColumns = { @JoinColumn( name = "id_gerente" ) }
    )
    private List<GerenteEntity> gerentes;

    public ContaEntityId getId() {
        return id;
    }

    public void setId(ContaEntityId id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public TipoContaEntity getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoContaEntity tipoConta) {
        this.tipoConta = tipoConta;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public List<MovimentacaoEntity> getMovimentacoes() {
        return movimentacoes;
    }

    public void setMovimentacoes(List<MovimentacaoEntity> movimentacoes) {
        this.movimentacoes = movimentacoes;
    }

    public List<GerenteEntity> getGerentes() {
        return gerentes;
    }

    public void setGerentes(List<GerenteEntity> gerentes) {
        this.gerentes = gerentes;
    }
}

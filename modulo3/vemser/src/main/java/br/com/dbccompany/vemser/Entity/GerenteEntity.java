package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.TipoGerenteEnum;
import br.com.dbccompany.vemser.Entity.Enum.TipoMovimentacaoEnum;

import javax.persistence.*;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn( name = "id" )
public class GerenteEntity extends UsuarioEntity{

    private int codigo;

    @Enumerated(EnumType.STRING)
    private TipoGerenteEnum tipoGerente;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable(name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn( name = "id_gerente" ) },
            inverseJoinColumns = { @JoinColumn( name = "id_conta" ), @JoinColumn( name = "id_tipo_conta" ) }
    )
    private List<ContaEntity> contas;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public TipoGerenteEnum getTipoGerente() {
        return tipoGerente;
    }

    public void setTipoGerente(TipoGerenteEnum tipoGerente) {
        this.tipoGerente = tipoGerente;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}

package br.com.dbccompany.vemser.Entity.Enum;

public enum EstadoCivilEnum {
    SOLTEIRO, CASADO, DIVORCIADO, UNIAO_ESTAVEL, VIUVO
}

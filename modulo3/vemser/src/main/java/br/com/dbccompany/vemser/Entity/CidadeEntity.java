package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class CidadeEntity {

    @Id
    @SequenceGenerator( name = "SEQ_CIDADE", sequenceName = "SEQ_CIDADE" )
    @GeneratedValue( generator = "SEQ_CIDADE", strategy = GenerationType.SEQUENCE )
    private int id;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_ESTADO" )
    private EstadoEntity estado;

    public List<EnderecoEntity> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<EnderecoEntity> enderecos) {
        this.enderecos = enderecos;
    }

    @OneToMany(mappedBy = "cidade")
    private List<EnderecoEntity> enderecos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public EstadoEntity getEstado() {
        return estado;
    }

    public void setEstado(EstadoEntity estado) {
        this.estado = estado;
    }
}

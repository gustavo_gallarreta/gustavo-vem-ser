package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;

@Entity
public class ConsolidacaoEntity {

    @Id
    @SequenceGenerator( name = "SEQ_CONSOLIDACAO", sequenceName = "SEQ_CONSOLIDACAO" )
    @GeneratedValue( generator = "SEQ_CONSOLIDACAO", strategy = GenerationType.SEQUENCE )
    private int id;
    private double saldoAtual = 0.0;
    private double saques = 0.0;
    private double depositos = 0.0;
    private int quantidadeCorrentistas = 0;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_AGENCIA" )
    private AgenciaEntity agencia;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSaldoAtual() {
        return saldoAtual;
    }

    public void setSaldoAtual(double saldoAtual) {
        this.saldoAtual = saldoAtual;
    }

    public double getSaques() {
        return saques;
    }

    public void setSaques(double saques) {
        this.saques = saques;
    }

    public double getDepositos() {
        return depositos;
    }

    public void setDepositos(double depositos) {
        this.depositos = depositos;
    }

    public int getQuantidadeCorrentistas() {
        return quantidadeCorrentistas;
    }

    public void setQuantidadeCorrentistas(int quantidadeCorrentistas) {
        this.quantidadeCorrentistas = quantidadeCorrentistas;
    }

    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }
}

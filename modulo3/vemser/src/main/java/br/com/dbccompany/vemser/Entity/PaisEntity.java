package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PaisEntity {

    @Id
    @SequenceGenerator( name = "PAIS_SEQ", sequenceName = "PAIS_SEQ" )
    @GeneratedValue( generator = "PAIS_SEQ", strategy = GenerationType.SEQUENCE )
    private int id;
    private String nome;

    public List<EstadoEntity> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadoEntity> estados) {
        this.estados = estados;
    }

    @OneToMany(mappedBy = "pais")
    private List<EstadoEntity> estados;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class BancoEntity {

    @Id
    @SequenceGenerator( name = "BANCO_SEQ", sequenceName = "BANCO_SEQ" )
    @GeneratedValue( generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE )
    private int id;
    private int codigo;
    private String nome;

    //@Column (name = "NOME_SOBRENOME", length = 100, columnDefinition = "NOME_SOBRENOME")
    //private String nomeSobrenome;

    @OneToMany(mappedBy = "banco")
    private List<AgenciaEntity> agencias;

    @OneToMany( mappedBy = "banco")
    private List<ContaClienteEntity> contas;

    public List<AgenciaEntity> getAgencias() {
        return agencias;
    }

    public void setAgencias(List<AgenciaEntity> agencias) {
        this.agencias = agencias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}

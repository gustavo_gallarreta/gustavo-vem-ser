package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class AgenciaEntity {

    @Id
    @SequenceGenerator( name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ" )
    @GeneratedValue( generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE )
    private int id;
    private int codigo;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_BANCO" )
    private BancoEntity banco;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_CONSOLIDACAO" )
    private ConsolidacaoEntity consolidacao;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "ID_ENDERECO" )
    private EnderecoEntity endereco;

    @OneToMany(mappedBy = "agencia")
    private List<ContaEntity> contas;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BancoEntity getBanco() {
        return banco;
    }

    public void setBanco(BancoEntity banco) {
        this.banco = banco;
    }

    public ConsolidacaoEntity getConsolidacao() {
        return consolidacao;
    }

    public void setConsolidacao(ConsolidacaoEntity consolidacao) {
        this.consolidacao = consolidacao;
    }

    public EnderecoEntity getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoEntity endereco) {
        this.endereco = endereco;
    }
}

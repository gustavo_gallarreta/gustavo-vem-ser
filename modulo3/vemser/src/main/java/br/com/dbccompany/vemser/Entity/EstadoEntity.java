package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.security.PrivilegedAction;
import java.util.List;

@Entity
public class EstadoEntity {

    @Id
    @SequenceGenerator( name = "SEQ_ESTADO", sequenceName = "SEQ_ESTADO" )
    @GeneratedValue( generator = "SEQ_ESTADO", strategy = GenerationType.SEQUENCE )
    private int id;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "ID_PAIS" )
    private PaisEntity pais;

    public List<CidadeEntity> getCidades() {
        return cidades;
    }

    public void setCidades(List<CidadeEntity> cidades) {
        this.cidades = cidades;
    }

    @OneToMany(mappedBy = "estado")
    private List<CidadeEntity> cidades;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public PaisEntity getPais() {
        return pais;
    }

    public void setPais(PaisEntity pais) {
        this.pais = pais;
    }
}


import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest {
    @Test
    public void agendaIniciaVazia() {
        AgendaContatos agenda = new AgendaContatos();
    assertEquals(0, agenda.getAgenda().size());

    }
    
    @Test
    public void adicionaItemNaAgenda() {
        AgendaContatos agenda = new AgendaContatos();
	agenda.adicionar("Marcos", "555555");
	agenda.adicionar("Mithrandir", "444444");
	assertEquals(2, agenda.getAgenda().size());
    }
    
    @Test
    public void retornaConsultaPorTelefone() {
        AgendaContatos agenda = new AgendaContatos();
	agenda.adicionar("Marcos", "555555");
	agenda.adicionar("Mithrandir", "444444");
	assertEquals(2, agenda.getAgenda().size());
	String esperado = agenda.consultar("Marcos");
	String esperado2 = agenda.consultar("Mithrandir");
	assertTrue("555555".equals(esperado));
	assertTrue("444444".equals(esperado2));
    }
    
    @Test
    public void retornaContatoPorNome(){
        AgendaContatos agenda = new AgendaContatos();
	agenda.adicionar("Marcos", "555555");
	agenda.adicionar("Mithrandir", "444444");
	assertEquals(2, agenda.getAgenda().size());
	String esperado = agenda.pesquisar("555555");
	String esperado2 = agenda.pesquisar("444444");
	assertTrue("Marcos,555555".equals(esperado));
	assertTrue("Mithrandir,444444".equals(esperado2));
    }
    
    @Test
    public void deveRetornarCSVOrdenadaAlfabeticamente(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Tiago", "000000");
	agenda.adicionar("Marcos", "555555");
	agenda.adicionar("Mithrandir", "444444");
	assertEquals(3, agenda.getAgenda().size());
	String esperado = "Marcos,555555\nMithrandir,444444\nTiago,000000";
	assertTrue(esperado.equals(agenda.csv()));
    }
}


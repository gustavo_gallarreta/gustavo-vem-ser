import java.util.*;
public class AgendaContatos {
    
    private HashMap<String, String> agenda;
    
    public AgendaContatos() {
        this.agenda = new HashMap<>();
    }
    
    public HashMap<String, String> getAgenda() {
        return this.agenda;
    }
    
    public void adicionar(String nome, String telefone) {
        this.agenda.put(nome, telefone);
    }
    
    public String consultar(String nome) {
        return agenda.containsKey(nome) ? agenda.get(nome) : null;
    }
    
    public String pesquisar(String telefone) {
        String chave = agenda.containsValue(telefone) ? obterChaves(telefone) : null;
        return chave != null ? (chave + "," + telefone) : null;
    }
    
    public String csv() {
        int aux = 0;
        StringBuilder agendaCSV = new StringBuilder();
        ArrayList<String> auxiliar = new ArrayList<>();
        
        Map<String, String> mavaInvertido = new TreeMap<String, String>(this.agenda);
        for (Map.Entry entry : mavaInvertido.entrySet()) {
            auxiliar.add(entry.getKey() + "," + entry.getValue());
        }
        ordenar(auxiliar);
        for (int i = 0; i < auxiliar.size(); i++) {
            agendaCSV.append(auxiliar.get(i));
            boolean devePularLinha = aux < (auxiliar.size() - 1);
            aux++;
            if( devePularLinha ) {
                agendaCSV.append("\n");
            }
        }
        return agendaCSV.toString();
    }
    
    private void ordenar(ArrayList<String> array) {
        
        int i, j;
        String aux;        
        for (i = array.size() - 1; i >= 1; i--) {
            for(j = 0; j < i; j++) {
                String atual = array.get(j);
                String proximo = array.get(j + 1);
                int comp = atual.compareTo(proximo);
                boolean deveTrocar = comp > 0;
                if(deveTrocar) {
                    aux = atual;
                    array.set(j, proximo);
                    array.set(j + 1, aux);
                }
            }
        }

    }
    
    private String obterChaves(String telefone) {
        for(String chave : this.agenda.keySet()) {
            if(this.agenda.get(chave).equals(telefone)) {
                return chave;
            }
        }
        return null;
    }
}

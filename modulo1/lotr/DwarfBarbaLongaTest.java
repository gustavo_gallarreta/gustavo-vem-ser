import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest {
    
    @Test
    public void chanceDePerderVida() {
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("Brostaeni");
        
        for(int i = 0; i < 12; i++) {
            dwarf.sofrerDano();
        }
        
        assertNotEquals(0.0, dwarf.getVida(), .01);
        assertNotEquals(Status.MORTO, dwarf.getStatus());
    }
}

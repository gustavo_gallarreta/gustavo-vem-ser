import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest {
    
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario(1);
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        assertEquals(item, inventario.getItens().get(0));
    }
    
    
    /*@Test
    public void adicionarUmItemOutroExemplo() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1, "Espada"));
        Item item = inventario.getItens()[0];
        assertEquals(1, item.getQuantidade());
        assertEquals("Espada", item.getDescricao());
    }*/
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario(1);
        Item item = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(item);
        inventario.adicionar(escudo);
        assertEquals(item, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }
    
    @Test
    public void adicionarUmItemEObter() {
        Inventario inventario = new Inventario(1);
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void adicionarUmItemERemover() {
        Inventario inventario = new Inventario(1);
        Item item = new Item(1, "Espada");
        inventario.adicionar(item);
        inventario.remover(0);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        String resultado = inventario.getDescricoes();
        assertEquals( "Espada,Escudo", resultado );
    }
    
    @Test
    public void getItemMaiorQuantidade() {
        Inventario inventario = new Inventario(1);
        Item lanca = new Item(1,"Lança");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }
    
    @Test
    public void getItemMaiorQuantidadeComMesmaQuantidade() {
        Inventario inventario = new Inventario(1);
        Item lanca = new Item(5,"Lança");
        Item espada = new Item(5, "Espada");
        Item escudo = new Item(2, "Escudo");
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
    
    /*@Test
    public void getItemMaiorQuantidadeComMesmaQuantidadeExemplo() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(5,"Lança"));
        inventario.adicionar(new Item(5, "Espada"));
        inventario.adicionar(new Item(2, "Escudo"));
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(inventario.obter(0), resultado);
    }*/
    
    @Test
    public void buscarApenasUmItem() {
        Inventario inventario = new Inventario(1);
        Item cafe = new Item(1, "cafe");
        inventario.adicionar(cafe);
        Item resultado = inventario.buscar(new String("cafe"));
        assertEquals(cafe, resultado);
    }
    
    @Test
    public void inverterDoisItens() {
        Inventario inventario = new Inventario(2);
        Item termica = new Item(1, "Termica de Café");
        Item energetico = new Item(1, "Energetico");
        inventario.adicionar(termica);
        inventario.adicionar(energetico);
        ArrayList<Item> resultado = inventario.inverter();
        
        assertEquals(energetico, resultado.get(0));
        assertEquals(termica, resultado.get(1));
        
        assertEquals(termica, inventario.obter(0));
        assertEquals(energetico, inventario.obter(1));
        
        assertEquals(2, resultado.size());
    }
    
    @Test
    public void ordenarComApenasUmItem() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarTotalmenteDesordenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarTotalmenteOrdenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarParcialmenteOrdenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenarQuantidadesIguais() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(escudo, cafe, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    @Test
    public void ordernarDescComApenasUmItem() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescTotalmenteOrdenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescTotalmenteDesordenado() {
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item cafe = new Item(3, "Térmica de café");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordernarDescParcialmenteDesordenado() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }    

    @Test
    public void ordernarDescQuantidadesIguais() {
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(escudo, cafe, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }
    
    
    
    
    
    
}

public class DwarfBarbaLonga extends Dwarf {
    DadoD6 dado = new DadoD6();
    
    public DwarfBarbaLonga( String nome ) {
        super(nome);
    }
    
    protected double calcularDano() {
        boolean chanceDeperderVida = dado.sortear() == 1 || dado.sortear() == 2 ? false : true; 
        
        if( chanceDeperderVida == false ){
            return 0;
        }
        return super.calcularDano();
    }
    
}
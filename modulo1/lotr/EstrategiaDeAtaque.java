import java.util.*;

public interface EstrategiaDeAtaque {
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
    
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes);
}

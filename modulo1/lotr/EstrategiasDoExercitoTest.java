import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiasDoExercitoTest {
    @Test
    public void elfosOrdenados() {
        EstrategiasDoExercito estrategia = new EstrategiasDoExercito();
        ArrayList<Elfo> elfosOrdenados = new ArrayList<>();
        Elfo elfoNoturno1 = new ElfoNoturno("LegolasNoturno1");
        Elfo elfoNoturno2 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfoNoturno3 = new ElfoNoturno("LegolasNoturno3");
        Elfo elfoVerde1 = new ElfoVerde("LegolasVerde1");
        Elfo elfoVerde2 = new ElfoVerde("LegolasVerde2");
        Elfo elfoVerde3 = new ElfoVerde("LegolasVerde3");
        elfosOrdenados.add(elfoNoturno1);
        elfosOrdenados.add(elfoNoturno2);
        elfosOrdenados.add(elfoNoturno3);
        elfosOrdenados.add(elfoVerde1);
        elfosOrdenados.add(elfoVerde2);
        elfosOrdenados.add(elfoVerde3);
        ArrayList<Elfo> exercitoOrdenado = estrategia.getOrdemDeAtaque(elfosOrdenados);
        assertEquals(elfoVerde1.getClass(),exercitoOrdenado.get(0).getClass());
        assertEquals(elfoVerde2.getClass(),exercitoOrdenado.get(1).getClass());
        assertEquals(elfoNoturno3.getClass(),exercitoOrdenado.get(5).getClass());        
    }
    
    @Test
    public void elfosIntercalados() {
        EstrategiasDoExercito estrategia = new EstrategiasDoExercito();
        ArrayList<Elfo> elfosIntercalados = new ArrayList<>();
        Elfo elfoNoturno1 = new ElfoNoturno("LegolasNoturno1");
        Elfo elfoNoturno2 = new ElfoNoturno("LegolasNoturno2");
        Elfo elfoNoturno3 = new ElfoNoturno("LegolasNoturno3");
        Elfo elfoNoturno4 = new ElfoNoturno("LegolasNoturno4");
        Elfo elfoVerde1 = new ElfoVerde("LegolasVerde1");
        Elfo elfoVerde2 = new ElfoVerde("LegolasVerde2");
        Elfo elfoVerde3 = new ElfoVerde("LegolasVerde3");
        elfosIntercalados.add(elfoNoturno1);
        elfosIntercalados.add(elfoNoturno2);
        elfosIntercalados.add(elfoNoturno3);
        elfosIntercalados.add(elfoNoturno4);
        elfosIntercalados.add(elfoVerde1);
        elfosIntercalados.add(elfoVerde2);
        elfosIntercalados.add(elfoVerde3);
        ArrayList<Elfo> exercitoOrdenado = estrategia.ataqueIntercalado(elfosIntercalados);
        assertEquals(elfoVerde1.getClass(),exercitoOrdenado.get(0).getClass());
        assertEquals(elfoNoturno1.getClass(),exercitoOrdenado.get(1).getClass());
        assertEquals(elfoVerde2.getClass(),exercitoOrdenado.get(2).getClass());
        assertEquals(6,exercitoOrdenado.size());
    }
}

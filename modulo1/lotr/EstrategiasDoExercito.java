import java.util.*;

public class EstrategiasDoExercito implements EstrategiaDeAtaque {
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );

    //ATAQUE PADRAO
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
       ArrayList<Elfo> elfosPorStatus = this.porStatus(atacantes);
       ArrayList<Elfo> elfosEmOrdem = new ArrayList<>();
       elfosEmOrdem.addAll(tipoDeElfos(TIPOS_PERMITIDOS.get(0), elfosPorStatus)); 
       elfosEmOrdem.addAll(tipoDeElfos(TIPOS_PERMITIDOS.get(1), elfosPorStatus));
       
       return elfosEmOrdem;
    }
    
    private ArrayList<Elfo> porStatus(ArrayList<Elfo> elfos) {
        ArrayList<Elfo> elfosPorStatus = new ArrayList<>();
        for(Elfo elfo : elfos) {
            boolean adicionarElfo = elfo.getStatus() != Status.MORTO;
            if(adicionarElfo){
                elfosPorStatus.add(elfo);
            }
        }
        return elfosPorStatus;
    }
    
    private ArrayList<Elfo> tipoDeElfos(Class tipo, ArrayList<Elfo> elfos) {
        ArrayList<Elfo> elfosPorAtaque = new ArrayList<>();
        for(Elfo elfo : elfos){
            if(tipo == elfo.getClass()) {
                elfosPorAtaque.add(elfo);
            }
        }
        return elfosPorAtaque;
    }
    
    // ATAQUE INTERCALADO
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes) {
        ArrayList<Elfo> elfosPorStatus = this.porStatus(atacantes);
        ArrayList<Elfo> elfosIntercados = new ArrayList<>();
        elfosIntercados.addAll(reduzirContingente(elfosPorStatus));
        
        return elfosIntercados;
    }
    
    private ArrayList<Elfo> reduzirContingente(ArrayList<Elfo> elfos) {
        ArrayList<Elfo> elfoVerde = new ArrayList<>();
        ArrayList<Elfo> elfoNoturno = new ArrayList<>();
        ArrayList<Elfo> contingenteReduzidoEIntercalado = new ArrayList<>();
        
        for(Elfo elfo : elfos){
            if(elfo.getClass() == TIPOS_PERMITIDOS.get(0)) {
                elfoVerde.add(elfo);
            }
            if(elfo.getClass() == TIPOS_PERMITIDOS.get(1)) {
                elfoNoturno.add(elfo);
            }
        }
        
        if(elfoVerde.size() > elfoNoturno.size()) {
            for(int i = 0; i < elfoVerde.size() - elfoNoturno.size(); i++) {
                elfoVerde.remove(i);
            }
        } else {
            for(int i = 0; i < elfoNoturno.size() - elfoVerde.size(); i++) {
                elfoNoturno.remove(i);
            }
        }
        
        contingenteReduzidoEIntercalado.addAll(this.intercalandoContingente(elfoVerde, elfoNoturno));
        
        return contingenteReduzidoEIntercalado;
    }
    
    private ArrayList<Elfo> intercalandoContingente(ArrayList<Elfo> verde, ArrayList<Elfo> noturno) {
        ArrayList<Elfo> contingenteIntercalado = new ArrayList<>();
        for(int i = 0; i < verde.size(); i++) {
            contingenteIntercalado.add(verde.get(i));
            contingenteIntercalado.add(noturno.get(i));
        }
        
        return contingenteIntercalado;
    }
}
